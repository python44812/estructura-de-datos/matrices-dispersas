from typing import List, Tuple

# Definimos una clase para el nodo de la lista enlazada
class Node:
    def __init__(self, data: int, col: int, next=None):
        self.data = data
        self.col = col
        self.next = next

# Definimos una función para convertir una matriz dispersa a listas enlazadas
def sparse_to_linked_lists(matrix: List[List[int]]) -> List[Node]:
    linked_lists = []
    for i, row in enumerate(matrix):
        head = Node(None, None)
        current = head
        for j, val in enumerate(row):
            if val != 0:
                current.next = Node(val, j)
                current = current.next
        linked_lists.append(head.next)
    return linked_lists

# Definimos una función para convertir una matriz dispersa a formato coordenado
def sparse_to_coordinate(matrix: List[List[int]]) -> List[Tuple[int, int, int]]:
    coordinate = []
    for i, row in enumerate(matrix):
        for j, val in enumerate(row):
            if val != 0:
                coordinate.append((i, j, val))
    return coordinate

# Ejemplo de una matriz dispersa
matrix = [
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 5, 3],
    [0, 8, 0, 0, 0, 0],
    [0, 0, 0, 4, 0, 0],
    [0, 0, 0, 0, 0, 0],
    [0, 0, 3, 0, 0, 2],
]

# Convertimos la matriz a listas enlazadas y formato coordenado
linked_lists = sparse_to_linked_lists(matrix)
coordinate = sparse_to_coordinate(matrix)

# Mostramos los resultados
print("Listas enlazadas:")
for i, ll in enumerate(linked_lists):
    current = ll
    values = []
    cols = []
    while current is not None:
        values.append(current.data)
        cols.append(current.col)
        current = current.next
    print(f"Row {i}: {values} (columns: {cols})")

print("\nFormato coordenado:")
for i, j, val in coordinate:
    print(f"({i}, {j}): {val}")
